# graphql-example


## scenario
GraphQL Instrumentation onCompleted; throwable is null when exception is thrown

## setup
1. PersonQuery throws IllegalArgumentException if id is less than or equal to 0
2. QueryTimer implements

        `beginExecution > onCompleted(ExecutionResult result, Throwable throwable)`

## usage
1. From command line;

        mvn spring-boot:run

2. With any rest client peform graphql query;

        POST http://localhost:8080/graphql
        
        With Body:
        {
            person(id: 0) {
                id
                firstname
                surname
            }
        }
        
3. Logs shows `hasThrowable: false` but IllegalArgumentException was thrown

         java.lang.IllegalArgumentException: id must be greater than 0
         ...
         INFO 12976 --- [nio-8080-exec-4] dev.krusty.QueryTimer : elapsedTime: 2 ms, hasThrowable: false

## expectations  

expected throwable in `onCompleted(ExecutionResult result, Throwable throwable)` to be the IllegalArgumentException exception