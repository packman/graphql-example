package dev.krusty.inmem;

import dev.krusty.Person;
import dev.krusty.PersonDao;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class InMemPersonDao implements PersonDao {

    private final Map<Integer, Person> peopleById;

    public InMemPersonDao(Person... people) {
        this.peopleById = Arrays.stream(people)
                .collect(Collectors.toMap(Person::getId, person -> person));
    }

    @Override
    public Person get(int id) {
        return peopleById.get(id);
    }
}
