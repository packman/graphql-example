package dev.krusty;

import dev.krusty.inmem.InMemPersonDao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PersonDaoConfig {

    @Bean
    public PersonDao personDao() {
        return new InMemPersonDao(
                new Person(1, "Homer", "Simpson"),
                new Person(2, "Ned","Flanders")
        );
    }
}
