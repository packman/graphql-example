package dev.krusty;

public interface PersonDao {
    Person get(int id);
}
