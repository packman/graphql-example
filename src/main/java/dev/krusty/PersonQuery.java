package dev.krusty;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PersonQuery implements GraphQLQueryResolver {

    @Autowired
    private PersonDao personDao;

    public Person person(int id) {

        if (id <= 0) {
            throw new IllegalArgumentException("id must be greater than 0");
        }

        return personDao.get(id);
    }
}
