package dev.krusty;

import graphql.execution.instrumentation.ChainedInstrumentation;
import graphql.execution.instrumentation.Instrumentation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;

@Configuration
public class GraphQLConfig {

    @Bean
    public Instrumentation instrumentation() {
        return new ChainedInstrumentation(Arrays.asList(
                new QueryTimer()
        ));
    }
}
