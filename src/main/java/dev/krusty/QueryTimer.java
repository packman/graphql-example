package dev.krusty;

import graphql.ExecutionResult;
import graphql.execution.instrumentation.InstrumentationContext;
import graphql.execution.instrumentation.SimpleInstrumentation;
import graphql.execution.instrumentation.SimpleInstrumentationContext;
import graphql.execution.instrumentation.parameters.InstrumentationExecutionParameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QueryTimer extends SimpleInstrumentation {

    private static final Logger logger = LoggerFactory.getLogger(QueryTimer.class);

    @Override
    public InstrumentationContext<ExecutionResult> beginExecution(InstrumentationExecutionParameters parameters) {

        final long startTime = System.currentTimeMillis();
        return new SimpleInstrumentationContext<ExecutionResult>() {
            @Override
            public void onCompleted(ExecutionResult result, Throwable throwable) {
                final long elapsedTime = System.currentTimeMillis() - startTime;
                final boolean hasThrowable = throwable != null;
                logger.info("elapsedTime: {} ms, hasThrowable: {}", elapsedTime, hasThrowable);
            }
        };
    }
}
